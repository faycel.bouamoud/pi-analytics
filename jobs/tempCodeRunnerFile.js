const fs = require('fs');
const dayjs = require('dayjs');
const csv_parser = require('csv-parse');
const csv=require("csvtojson");
const db = require('../model/db');

const enau = require('../data/enau/result.json');
const ensi = require('../data/ensi/ensi.json');
const supcom = require('../data/supcom/supcom.json');
const tekup = require('../data/tekup/result.json');
const uc = require('../data/uc/result.json');
const enicar = require('../data/enau/result.json');
//const { schoolModel } = require('../model');
const esprit = require('../data/esprit/esprit.json');
const utils = require('./utils');
const model = require('../model');

const skills = new Set();
[...esprit, ...ensi, ...supcom, ...tekup, ...uc, ...enicar, ...enau]
    .forEach(async (el) => {
        if (el.general) {
            let school = await utils.getSchoolByName(el.general.school);
            if (!school) school = await model.SchoolModel({ name: el.general.school }).save();
            const student = await utils.getStudentByFullName(el.general.firstName, el.general.lastName)
            el.skills && el.skills.forEach(s => {
                model.SkillsModel({ name: s.name, institute: school._id, student: student && student._id }).save();
            });
        }
    });