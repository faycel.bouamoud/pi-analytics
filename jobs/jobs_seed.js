const fs = require('fs');
const dayjs = require('dayjs');
const csv_parser = require('csv-parse');
const csv=require("csvtojson");
const db = require('../model/db');

const enau = require('../data/enau/result.json');
const esprit = require('../data/esprit/result.json');
const ensi = require('../data/ensi/ensi.json');
const supcom = require('../data/supcom/supcom.json');
const tekup = require('../data/tekup/result.json');
const uc = require('../data/uc/result.json');
const enicar = require('../data/enau/result.json');
const espritt = require('../data/esprit/esprit.json');
const utils = require('./utils');
const model = require('../model');

const months = {
    "janv.": 0,
    "févr.": 1,
    "mars": 2,
    "avr.": 3,
    "mai": 4,
    "juin": 5,
    "juil.": 6,
    "août": 7,
    "sept.": 8,
    "oct.": 9,
    "nov.": 10,
    "déc.": 11
};

const acc = new Set();
const jobs = [...esprit, ...ensi, ...supcom, ...tekup, ...uc, ...enicar, ...enau]
    .reduce((accc, curr) => {
        curr.jobs.forEach(async (element) => {
            const start = element.dateRange &&
                (
                    element.dateRange
                    .split(' – ')[0]
                    .split(' ')
                    .length > 1 ?
                    dayjs().set('month', months[element.dateRange.split(' – ')[0].split(' ')[0]]).set('year', element.dateRange.split(' – ')[0].split(' ')[1]) :
                    dayjs().set('year', element.dateRange.split(' – ')[0].split(' ')[0])
                );
            const end = element.dateRange &&
                (
                    element.dateRange
                    .split(' – ')[1]
                    .split(' ')
                    .length > 1 ?
                    dayjs().set('month', months[element.dateRange.split(' – ')[0].split(' ')[0]]).set('year', element.dateRange.split(' – ')[0].split(' ')[1]) :
                    isNaN(element.dateRange.split(' – ')[0].split(' ')[0]) && dayjs() || dayjs().set('year', element.dateRange.split(' – ')[0].split(' ')[0])
                );
            const duration = dayjs(end).diff(dayjs(start), 'month');
            const company = await utils.getComapantByName(element.companyName) ;
            if (company) {
                model.JobModel({
                    title: element.jobTitle,
                    company: company._id,
                    dateStart: start && start.toDate(),
                    duration,
                    student: utils.getStudentByFullName(curr.general.firstName, curr.general.lastName),
                }).save();
            } else {
                const co = await model.CompanyModel({ name: element.companyName, location: element.location });
                model.JobModel({
                    title: element.jobTitle,
                    company: co._id,
                    dateStart: start && start.toDate(),
                    duration,
                    student: utils.getStudentByFullName(curr.general.firstName, curr.general.lastName),
                }).save();
            }
        });
    }, []);