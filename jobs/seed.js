const fs = require('fs');
const dayjs = require('dayjs');
const csv_parser = require('csv-parse');
const csv=require("csvtojson");
const db = require('../model/db');

const enau = require('../data/enau/result.json');
const esprit = require('../data/esprit/result.json');
const ensi = require('../data/ensi/ensi.json');
const supcom = require('../data/supcom/supcom.json');
const tekup = require('../data/tekup/result.json');
const uc = require('../data/uc/result.json');
const enicar = require('../data/enau/result.json');
//const { schoolModel } = require('../model');
const espritt = require('../data/esprit/esprit.json');

console.log(espritt.length);


const months = {
    "janv.": 0,
    "févr.": 1,
    "mars": 2,
    "avr.": 3,
    "mai": 4,
    "juin": 5,
    "juil.": 6,
    "août": 7,
    "sept.": 8,
    "oct.": 9,
    "nov.": 10,
    "déc.": 11
};

const acc = new Set();
const jobs = [...esprit, ...ensi, ...supcom, ...tekup, ...uc, ...enicar, ...enau]
    .reduce((accc, curr) => {
        curr.jobs.forEach(element => {
            const start = element.dateRange &&
                (
                    element.dateRange
                    .split(' – ')[0]
                    .split(' ')
                    .length > 1 ?
                    dayjs().set('month', months[element.dateRange.split(' – ')[0].split(' ')[0]]).set('year', element.dateRange.split(' – ')[0].split(' ')[1]) :
                    dayjs().set('year', element.dateRange.split(' – ')[0].split(' ')[0])
                );
            const end = element.dateRange &&
                (
                    element.dateRange
                    .split(' – ')[1]
                    .split(' ')
                    .length > 1 ?
                    dayjs().set('month', months[element.dateRange.split(' – ')[0].split(' ')[0]]).set('year', element.dateRange.split(' – ')[0].split(' ')[1]) :
                    isNaN(element.dateRange.split(' – ')[0].split(' ')[0]) && dayjs() || dayjs().set('year', element.dateRange.split(' – ')[0].split(' ')[0])
                );
            const duration = dayjs(end).diff(dayjs(start), 'month');
            acc.add({
                title: element.jobTitle,
                company: element.companyName,
                dateStart: start && start.toDate(),
                duration
            })
        });
    }, []);

console.log(acc);

const studetns = esprit
    .filter(el =>
        el.general.school && (el.general.school.includes('esprit') ||
            el.general.school.includes('ESPRIT') ||
            el.general.school.includes('Esprit'))
    )
    .map(el => ({
        firstname: el.general.firstName,
        lastname: el.general.lastName,
        // jobs: el.jobs.map(j => ({ companyName: j.companyName, jobTitle: j.jobTitle, location: j.location })),
        school: el.general.school,
        address: el.general.location,
    }));

const schools = new Set();
[...esprit, ...tekup, ...uc, ...ensi, ...supcom, ...enicar].forEach(el => {
    el.schools && el.schools.forEach(s => {
        schools.add({
            name: s.schoolName
        });
    });
});

const teachers = new Set();
[...esprit, ...ensi, ...supcom, ...tekup, ...uc, ...enicar, ...enau]
.forEach(el => {
    if(el.general && (el.general.headline.toLowerCase().includes('teacher')
        || el.general.headline.toLowerCase().includes('enseignant')
        || el.general.headline.toLowerCase().includes('professeur')
        || el.general.headline.toLowerCase().includes('teaching')
        || el.general.headline.toLowerCase().includes('professor')
        || el.general.headline.toLowerCase().includes('technologue')
    ))
        teachers.add({
            firstname: el.general.firstName,
            lastname: el.general.lastName,
            company: el.general.company,
        })
});

console.log(teachers);

const companies = new Set();
[...esprit, ...ensi, ...supcom, ...tekup, ...uc, ...enicar, ...enau]
    .forEach(el => {
        el.jobs.forEach(j => {
            companies.add({ name: j.companyName, location: j.location });
        });
    });
console.log(companies);

const projects = new Set();
[...esprit, ...ensi, ...supcom, ...tekup, ...uc, ...enicar, ...enau]
    .filter(el => el.general && (el.general.headline.toLowerCase().includes('ceo') || el.general.headline.toLowerCase().includes('co-founder')))
    .forEach(el => {
        projects.add({ name: el.general.company });
    });
console.log(projects);

const skills = new Set();
[...esprit, ...ensi, ...supcom, ...tekup, ...uc, ...enicar, ...enau]
    .forEach(el => {
        el.skills && el.skills.forEach(s => {
            skills.add({ name: s.name, school: el.general && el.general.school, student: el.general.fullName });
        })
    });
console.log(skills);

