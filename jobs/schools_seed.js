const fs = require('fs');
const dayjs = require('dayjs');
const csv_parser = require('csv-parse');
const csv=require("csvtojson");
const db = require('../model/db');
const model = require('../model');

const enau = require('../data/enau/result.json');
const ensi = require('../data/ensi/ensi.json');
const supcom = require('../data/supcom/supcom.json');
const tekup = require('../data/tekup/result.json');
const uc = require('../data/uc/result.json');
const enicar = require('../data/enau/result.json');
const esprit = require('../data/esprit/esprit.json');

const schools = new Set();
[...esprit, ...tekup, ...uc, ...ensi, ...supcom, ...enicar].forEach(el => {
    el.schools && el.schools.forEach(s => {
        new model.SchoolModel({
            name: s.schoolName
        }).save();
    });
});