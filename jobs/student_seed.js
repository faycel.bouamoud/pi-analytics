const fs = require('fs');
const dayjs = require('dayjs');
const csv_parser = require('csv-parse');
const csv=require("csvtojson");
const db = require('../model/db');

const enau = require('../data/enau/result.json');
const esprit = require('../data/esprit/result.json');
const ensi = require('../data/ensi/ensi.json');
const supcom = require('../data/supcom/supcom.json');
const tekup = require('../data/tekup/result.json');
const uc = require('../data/uc/result.json');
const enicar = require('../data/enau/result.json');
//const { schoolModel } = require('../model');
const espritt = require('../data/esprit/esprit.json');
const utils = require('./utils');
const model = require('../model');

const studetns = [...esprit, ...ensi, ...supcom, ...tekup, ...uc, ...enicar, ...enau]
    .map(async (el) => {
        if (el.general) {
            let school = el.general.school && await utils.getSchoolByName(el.general.school);
            if (!school) school = el.general.school && await model.SchoolModel({ name: el.general.school });
            const student = {
                firstname: el.general.firstName,
                lastname: el.general.lastName,
                institute: school && school._id,
                address: el.general.location,
            };
            model.StudentModel(student).save();
        }
    });
