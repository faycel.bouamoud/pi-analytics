const fs = require('fs');
const dayjs = require('dayjs');
const csv_parser = require('csv-parse');
const csv=require("csvtojson");
const db = require('../model/db');

const enau = require('../data/enau/result.json');
const ensi = require('../data/ensi/ensi.json');
const supcom = require('../data/supcom/supcom.json');
const tekup = require('../data/tekup/result.json');
const uc = require('../data/uc/result.json');
const enicar = require('../data/enau/result.json');
//const { schoolModel } = require('../model');
const esprit = require('../data/esprit/esprit.json');
const model = require('../model');
const utils = require('./utils');

[...esprit, ...ensi, ...supcom, ...tekup, ...uc, ...enicar, ...enau]
.forEach(async (el) => {
    if(el.general && (el.general.headline.toLowerCase().includes('teacher')
        || el.general.headline.toLowerCase().includes('enseignant')
        || el.general.headline.toLowerCase().includes('professeur')
        || el.general.headline.toLowerCase().includes('teaching')
        || el.general.headline.toLowerCase().includes('professor')
        || el.general.headline.toLowerCase().includes('technologue')
    )) {
        let school = await utils.getSchoolByName(el.general.company);
        if (!school) school = await model.SchoolModel({ name: el.general.company }).save();
        model.TeacherModel({
            firstname: el.general.firstName,
            lastname: el.general.lastName,
            institue: school._id,
        }).save();
    }
});