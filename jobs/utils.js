const model = require('../model');

function getSchoolByName(name) {
    return name && model.SchoolModel.findOne({ name: { '$regex': name, '$options': 'i' } });
}

function getComapantByName(name) {
    return name && model.CompanyModel.findOne({ name: {'$regex': name, '$options': 'i' } });
}

function getStudentByFullName(firstname, lastname) {
    return firstname && lastname && model.StudentModel.findOne({ firstname, lastname });
}

module.exports = {
    getSchoolByName,
    getComapantByName,
    getStudentByFullName,
}