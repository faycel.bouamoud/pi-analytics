const fs = require('fs');
const dayjs = require('dayjs');
const csv_parser = require('csv-parse');
const csv=require("csvtojson");
const db = require('../model/db');

const enau = require('../data/enau/result.json');
const ensi = require('../data/ensi/ensi.json');
const supcom = require('../data/supcom/supcom.json');
const tekup = require('../data/tekup/result.json');
const uc = require('../data/uc/result.json');
const enicar = require('../data/enau/result.json');
const esprit = require('../data/esprit/esprit.json');

const model = require('../model');


const companies = new Set();
[...esprit, ...ensi, ...supcom, ...tekup, ...uc, ...enicar, ...enau]
    .forEach(el => {
        el.jobs.forEach(j => {
            const query = { name: { $regex: j.companyName, $options: 'i' } };
            update = { '$set': { name: j.companyName, location: j.location } };
            options = { upsert: true, new: true, setDefaultsOnInsert: true };

            // Find the document
            model.CompanyModel.findOneAndUpdate(query, update, options, function(error, result) {
                if (error) return;
                if(!result) {
                    new model.CompanyModel({ name: j.companyName, location: j.location }).save();
                }
            });
        });
    });