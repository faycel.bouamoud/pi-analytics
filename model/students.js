const mongoose = require('mongoose');

const StudentSchema = new mongoose.Schema({
    firstname: {
        type: String,
    },
    lastname: {
        type: String,
    },
    email: {
        type: String,
    },
    has_degree: {
        type: Boolean,
    },
    institute: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'schools',
    },
    address: {
        type: String,
    },
    jobs: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'jobs',
    }]
});

module.exports = mongoose.model('students', StudentSchema);