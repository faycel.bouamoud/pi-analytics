const mongoose = require('mongoose');

const FieldsSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    institute: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'schools',
    },
    student: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'students',
    }
});

module.exports = mongoose.model('skills', FieldsSchema);