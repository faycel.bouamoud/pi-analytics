const mongo = require('mongoose');

mongo.connect('mongodb://localhost/esprit_pi', { useNewUrlParser: true })
    .then(data => {
        console.log('Connected successfully');
    });

module.exports = mongo.connection;