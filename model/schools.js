const mongoose = require('mongoose');

const SchoolsSchema = new mongoose.Schema({
    name: {
        type: String,
    },
});

module.exports = mongoose.model('schools', SchoolsSchema);