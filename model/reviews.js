const mongoose = require('mongoose');

const ReviewsSchema = new mongoose.Schema({
    body: {
        type: String,
    },
    positive: {
        type: Boolean,
    },
    student: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'students',
    },
    institute: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'schools',
    },
});

module.exports = mongoose.model('reviews', ReviewsSchema);