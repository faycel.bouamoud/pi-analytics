const mongoose = require('mongoose');

const JobsSchema = new mongoose.Schema({
    title: {
        type: String,
    },
    company: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'companies',
    },
    field: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'fields',
    },
});

module.exports = mongoose.model('jobs', JobsSchema);