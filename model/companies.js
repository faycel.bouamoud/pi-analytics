const mongoose = require('mongoose');
const CompaniesType = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
    },
    location: {
        type: String,
    },
});

module.exports = mongoose.model('companies', CompaniesType);