const mongoose = require('mongoose');

const ProjectsSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    year_foundation: {
        type: Number,
    },
    student_id: {
        type: mongoose.Schema.Types.ObjectId,
    },
});

module.exports = mongoose.model('projects', ProjectsSchema);