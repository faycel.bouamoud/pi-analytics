const CompanyModel = require('./companies');
const SkillsModel = require('./skills');
const CountryModel =require('./countries');
const JobModel = require('./jobs');
const ProjectModel = require('./jobs');
const ReviewModel = require('./reviews');
const SchoolModel = require('./schools');
const StudentModel = require('./students');
const TeacherModel = require('./teachers');

module.exports = {
    CompanyModel,
    SkillsModel,
    CountryModel,
    JobModel,
    ProjectModel,
    ReviewModel,
    SchoolModel,
    StudentModel,
    TeacherModel,
};