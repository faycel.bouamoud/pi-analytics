const mongoose = require('mongoose');

const TeachersSchema = new mongoose.Schema({
    firstname: {
        type: String,
    },
    lastname: {
        type: String,
    },
    nationality: {
        type: String,
    },
    email: {
        type: String,
    },
    institue: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'schools',
    }
});

module.exports = mongoose.model('teachers', TeachersSchema);